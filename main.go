package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/mash/gokmeans"
)

var observations = []gokmeans.Node{{4}, {5}, {6}, {8}, {10}, {12}, {15}, {0}, {-1}}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s k\n", os.Args[0])
		return
	}

	k, err := strconv.Atoi(os.Args[1])
	if err != nil {
		panic(err)
	}

	if success, centroids := gokmeans.Train(observations, k, 50); success {
		fmt.Println("The centroids are following:")
		for _, centroid := range centroids {
			fmt.Println(centroid)
		}

		fmt.Println("The clusters are following:")
		for _, observ := range observations {
			index := gokmeans.Nearest(observ, centroids)
			fmt.Println(observ, "belongs to cluster", index+1)
		}
	}

}
